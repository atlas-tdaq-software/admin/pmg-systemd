Name:           pmgserver
Version:        v1.0.4
Release:        1%{?dist}
Summary:        TDAQ Process Manager

# Group:          
License:        Apache 2.0

# URL:            
Source0:         https://gitlab.cern.ch/atlas-tdaq-software/admin/pmg-systemd/-/archive/%{version}/pmg-systemd-%{version}.tar.bz2 
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch

Requires:      sudo
Requires(pre): /usr/sbin/useradd /usr/sbin/groupadd /usr/bin/getent

%description
The TDAQ Process Manager.

The process manager allows a TDAQ partition to start processes
on the local host.

%prep
%setup -q -n pmg-systemd-%{version}
%build
%install

rm -rf %{buildroot}

mkdir -p %{buildroot}/usr/libexec %{buildroot}/usr/lib/systemd/system %{buildroot}/%{_sysconfdir}/sudoers.d %{buildroot}/%{_mandir}/man8
install usr/libexec/launch_pmg %{buildroot}/%{_libexecdir}
install -m 644 usr/lib/systemd/system/pmgserver@.service %{buildroot}/usr/lib/systemd/system/
install -m 644 usr/lib/firewalld/services/tdaq-corba.xml %{buildroot}/usr/lib/firewalld/services/
install -m 644 etc/sudoers.d/pmg %{buildroot}/%{_sysconfdir}/sudoers.d/
install -m 644 etc/omniORB.cfg %{buildroot}/%{_sysconfdir}/
install -m 644 usr/share/man/man8/pmgserver.8 %{buildroot}/%{_mandir}/man8/

%clean
#rm -rf $RPM_BUILD_ROOT

%pre
/usr/bin/getent group zp > /dev/null    || /usr/sbin/groupadd -g 1307 zp
/usr/bin/getent passwd tdaqsw > /dev/null || /usr/sbin/useradd -u 42056 -g zp tdaqsw

%files
%defattr(-,root,root,-)
/usr/lib/systemd/system/pmgserver@.service
/usr/lib/firewalld/services/tdaq-corba.xml
%{_libexecdir}/launch_pmg
%{_mandir}/man8/pmgserver.8.gz
%{_sysconfdir}/sudoers.d/pmg
%{_sysconfdir}/omniORB.cfg

%changelog
