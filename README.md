# ATLAS TDAQ pmgserver as a systemd service

This package is used to to create an RPM that can
be installed on a private machine at CERN. 

It makes the TDAQ pmgserver available as a 
systemd service. You can control it via the
usual systemctl commands and see the 
output with journalctl.

```shell
sudo systemctl start pmgserver@nightly
sudo systemctl start pmgserver@tdaq-09-04-00
systemctl status pmgserver@*
sudo journalctl -u pmgserver@nightly
```

To have the pmgserver for a given release
start automatically after reboot:

```shell
sudo systemctl enable pmgserver@tdaq-09-04-00
```

The `/etc/sudoers.d/pmg` file can be used to
further restrict the target users on a given
machine.

## Customizing pmgserver resources

You can create drop-in files to customize
either all pmgservers or only a specific one

```shell
sudo systemctl edit pmgserver@
sudo systemctl edit pmgserver@nightly
```

Some examples are indicated in the comments.
E.g. to allow core dumps the drop-in file
should contain

```
[Service]
LimitCORE=infinite
```

You can as well restrict the resources used
by the processes started through the pmgserver
e.g restrict them to a certain number of CPUs
or a maximum memory size. See

```shell
man systemd.exec
man systemd.resource-control
```

## Customizing the firewall

If you run the CentOS firewall service
(recommended), you should execute the 
following:

```shell
sudo firewall-cmd --add-service=tdaq-corba --permanent
sudo firewall-cmd --reload
```

